# python

# --------------------------------------------------------------------------------
# dcCommandTemplate.py
# Author: David Cuellar
# Website: http://www.davidcuellar.es
# Email: david.cuellar@gmail.com
# Date: 20 September 2015
# Version: 1.0
#
# Release Notes:
# 1.0- First release
#
# Autodesk Maya Command template API 2.0
# Copyright (C) 2015  David Cuellar
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
# --------------------------------------------------------------------------------


import maya.api.OpenMaya as om


def maya_useNewAPI():
    """
    The presence of this function tells Maya that the plugin produces, and
    expects to be passed, objects created using the Maya Python API 2.0.
    """
    pass


_pluginName_ = 'templateCommand'
_author_ = 'David Cuellar'
_version_ = '1.0'
_apiVersion_ = 'Any'


# --------------------------------------------------------------------------------
# arguments flags
# --------------------------------------------------------------------------------
helpFlag = '-h'
helpFlagLong = '-help'

testFlag = '-t'
testFlagLong = 'test'

# --------------------------------------------------------------------------------
# help information
# --------------------------------------------------------------------------------

helpText = ''
helpText += '\n Description: This is a template of a command using Maya API 2.0.'
helpText += '\n'
helpText += '\n Flags: {0}   -h        -help            <n/a>        help message'
helpText += '\n                          -t        -test            <string>     testFlag'
helpText += '\n Usage: Execute the command with the following arguments:'
helpText += '\n {0} -t  <testFlag> '.format(_pluginName_)


# --------------------------------------------------------------------------------

class templateCommand(om.MPxCommand):
    def __init__(self):
        om.MPxCommand.__init__(self)

    def doIt(self, argList):
        argData = om.MArgDatabase(self.syntax(), argList)
        # help flag
        if argData.isFlagSet(helpFlag):
            self.setResult(helpText)
            return
        if argData.isFlagSet(testFlag):
            print argData.flagArgumentString(testFlag, 0)

    def redoIt(self, argList):
        self.doIt(argList)

    def undoIt(self):
        self.undoIt()

    def isUndoable(self):
        return True

# --------------------------------------------------------------------------------


def cmdCreator():
    return templateCommand()


def syntaxCreator():
    syn = om.MSyntax()
    syn.addFlag(helpFlag, helpFlagLong)
    syn.addFlag(testFlag, testFlagLong, om.MSyntax.kString)
    return syn


def initializePlugin(obj):
    plugin = om.MFnPlugin(obj, _author_, _version_, _apiVersion_)
    try:
        plugin.registerCommand(_pluginName_, cmdCreator, syntaxCreator)
    except:
        raise RuntimeError, 'Failed to register command'


def uninitializePlugin(obj):
    plugin = om.MFnPlugin(obj)
    try:
        plugin.deregisterCommand(_pluginName_)
    except:
        raise RuntimeError, ('Failed to unregister command: {0}\n'.format(_pluginName_))
